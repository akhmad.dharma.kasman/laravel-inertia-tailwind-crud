<?php

namespace App\Http\Controllers;

use App\Models\Nasabah;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Requests\StoreNasabahRequest;
use Illuminate\Support\Carbon;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nasabah = Nasabah::paginate(10);
        return Inertia::render('Index', ['nasabah' => $nasabah]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Nasabah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNasabahRequest $request)
    {
        //Nasabah::create($request->all());
        Nasabah::create([
            'noktp' => $request->noktp,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'tptlhr' => $request->tptlhr,
            'tgllhr' => Carbon::parse($request->tgllhr)->format('Y-m-d'),
            'status' => $request->status,
            'hp' => $request->status,
            'npwp' => $request->npwp,
            'warganegara' => $request->warganegara,
            'kelas' => $request->kelas,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kodepos' => $request->kodepos,
            'telp_rmh' => $request->telp_rmh,
            'kel_desa' => $request->kel_desa,
            'nokk' => $request->nokk,
            'status_kel' => $request->status_kel,
            'jml_anak' => $request->jml_anak,
            'bank' => $request->bank,
            'norek' => $request->norek,
            'nmrek' => $request->nmrek
        ]);
        return redirect('/nasabah')
        ->with([
            'message'=> 'Peserta Created!!',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nasabah  $nasabah
     * @return \Illuminate\Http\Response
     */
    public function show(Nasabah $nasabah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nasabah  $nasabah
     * @return \Illuminate\Http\Response
     */
    public function edit(Nasabah $nasabah)
    {
        return Inertia::render('Nasabah', ['nasabah' => $nasabah]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Nasabah  $nasabah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nasabah $nasabah)
    {
        //$nasabah->fill($request->all())->save();
        Nasabah::where('id', $nasabah->id)
            ->update([
                'noktp' => $request->noktp,
                'nama' => $request->nama,
                'jenkel' => $request->jenkel,
                'tptlhr' => $request->tptlhr,
                'tgllhr' => Carbon::parse($request->tgllhr)->format('Y-m-d'),
                'status' => $request->status,
                'hp' => $request->status,
                'npwp' => $request->npwp,
                'warganegara' => $request->warganegara,
                'kelas' => $request->kelas,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kodepos' => $request->kodepos,
                'telp_rmh' => $request->telp_rmh,
                'kel_desa' => $request->kel_desa,
                'nokk' => $request->nokk,
                'status_kel' => $request->status_kel,
                'jml_anak' => $request->jml_anak,
                'bank' => $request->bank,
                'norek' => $request->norek,
                'nmrek' => $request->nmrek

            ]);

        return redirect('/nasabah/' . $nasabah->id . '/edit')
        ->with([
            'message'=> 'Nasabah Updated!!',
            'type' => 'success'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Nasabah  $nasabah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nasabah = Nasabah::findOrfail($id);
        $nasabah->delete();        
        return redirect('/nasabah')
        ->with([
            'message'=> 'Peserta Berhasil dihapus!!',
            'type' => 'success'
        ]);
    }
}
