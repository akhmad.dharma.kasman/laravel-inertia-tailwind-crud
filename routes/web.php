<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\NasabahController;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});

Route::get('/testing',[TestingController::class, 'index']);
/*Route::get('/product',[ProductController::class, 'index']);
Route::post('/product',[ProductController::class, 'store']);
Route::get('/product/{product}/edit',[ProductController::class, 'edit']);
Route::put('/product/{product}',[ProductController::class, 'update']);*/

Route::resource('product', ProductController::class);
Route::resource('nasabah', NasabahController::class);


require __DIR__ . '/auth.php';
