<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Nasabah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nasabahs', function (Blueprint $table) {
            $table->id();
            $table->string('noktp')->nullable();
            $table->string('nama')->nullable();
            $table->integer('jenkel')->nullable();
            $table->string('tptlhr')->nullable();
            $table->date('tgllhr')->nullable();
            $table->string('status')->nullable();
            $table->string('hp')->nullable();
            $table->string('npwp')->nullable();
            $table->string('warganegara')->nullable();
            $table->string('kelas')->nullable();
            $table->string('email')->nullable();
            $table->string('alamat')->nullable();
            $table->string('rt')->nullable();
            $table->string('rw')->nullable();
            $table->string('kodepos')->nullable();
            $table->string('telp_rmh')->nullable();
            $table->string('kel_desa')->nullable();
            $table->string('nokk')->nullable();
            $table->string('status_kel')->nullable();
            $table->string('jml_anak')->nullable();
            $table->string('bank')->nullable();
            $table->string('norek')->nullable();
            $table->string('nmrek')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nasabahs');
    }
}
